Title : Project Calculatro RPN 
Grade : M1 Computer science 
Developper : CHAHI RABIE ALA EDDINE
@ : rabie_chahi@yahoo.fr

1 Context : 

Ce programme implémente une calculatrice à notation polonaise inverse.

2 usability : 

You can add, subtract, multiply and divide numbers contained in a stack that manages the calculation process.

3 Reverse Polish notation :

This way of writing arithmetic expressions is relatively intuitive and therefore allows you to write these expressions postfixically

4 application : 

This calculator performs the calculations according to the FILO input order of a stack, displays the current state of the stack at each step, the postfix and infix notation of the current arithmetic expression, and the overall result.

5 Example :

For example, the expression "3 × (4 + 7)" can be written as NPI in the form "4 {Ent} 1 7 + 3 ×", or in the form "3 {Ent} 4 {Ent} 7 + ×".

